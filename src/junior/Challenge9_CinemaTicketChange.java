//The new "Avengers" movie has just been released! There are a lot of people at the cinema box office standing in a huge line.
// Each of them has a single 100, 50 or 25 dollar bill. An "Avengers" ticket costs 25 dollars.
//
//        Vasya is currently working as a clerk. He wants to sell a ticket to every single person in this line.
//
//        Can Vasya sell a ticket to every person and give change if he initially has no money and sells the tickets strictly in the order people queue?
//
//        Return YES, if Vasya can sell a ticket to every person and give change with the bills he has at hand at that moment. Otherwise return NO.
//
//        Examples:
//        Line.Tickets(new int[] {25, 25, 50}) // => YES
//        Line.Tickets(new int[]{25, 100}) // => NO. Vasya will not have enough money to give change to 100 dollars
//        Line.Tickets(new int[] {25, 25, 50, 50, 100}) // => NO. Vasya will not have the right bills to give 75 dollars of change (you can't make two bills of 25 from one of 50)

//test doesnt work because of static variable
package junior;

public class Challenge9_CinemaTicketChange {

    public static int[] vasiasBills = {0,0,0};

    public static void main (String[] args)
    {
        System.out.println(Tickets(new int[] {25, 100}));

    }


    public static String Tickets(int[] peopleInLine)
    {
        for (int i = 0; i <peopleInLine.length; i++)
        {
            if (!processPayment(peopleInLine[i]))
            {
                return "NO";
            }
        }
        return "YES";
    }


    public static int[] giveVasiaMoney(int moneyBill)
    {
        switch (moneyBill)
        {
            case 25:
                vasiasBills[0] +=1;
                break;
            case 50:
                vasiasBills[1] +=1;
                break;
            case 100:
                vasiasBills[2] +=1;
                break;
            default:
                return vasiasBills;
        }
        return vasiasBills;
    }

  public static boolean processPayment(int billProvided)
  {
      switch (billProvided) {
          case 25:
              giveVasiaMoney(25);
              return true;
          case 50:
              //return 25
              if (vasiasBills[0] >= 1) {
                  vasiasBills[0] -= 1;
                  giveVasiaMoney(50);
                  return true;
              }
              return false;
          case 100:
          {
              //return 50 and 25;
              if (vasiasBills[0] >=1 && vasiasBills[1]>=1)
              {
                  vasiasBills[0] -=1;
                  vasiasBills[1] -=1;
                  giveVasiaMoney(100);
                  return true;
              }
              else if (vasiasBills[0] >=3) // return 3x25
              {
                  vasiasBills[0] -=3;
                  giveVasiaMoney(100);
                  return true;
              }
              else return false;
          }
      }
      return false;
  }

}
