package junior.tests;

import junior.Challenge13_StringPartMoveAy;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Test_Challenge13 {

    @Test
    public void FixedTests() {
        assertEquals("igPay atinlay siay oolcay", Challenge13_StringPartMoveAy.pigIt("Pig latin is cool"));
        assertEquals("hisTay siay ymay tringsay", Challenge13_StringPartMoveAy.pigIt("This is my string"));
    }

}
