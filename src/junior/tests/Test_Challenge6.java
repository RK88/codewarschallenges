package junior.tests;

import junior.Challenge6_TravelNorthSouthEtc;
import org.junit.Assert;
import org.junit.Test;

public class Test_Challenge6 {



    @Test
    public void Challenge1_SumOfDividablesFromUpTo10_ShouldBe23()
    {
        String[] expected = {"WEST"};
        Assert.assertArrayEquals(expected , Challenge6_TravelNorthSouthEtc.dirReduc(new String[]{"NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH", "WEST"}));
    }

}
