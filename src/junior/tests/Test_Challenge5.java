package junior.tests;

import junior.Challenge5_IsPrime;
import org.junit.Assert;
import org.junit.Test;

public class Test_Challenge5 {

    @Test
    public void Challenge1_SumOfDividablesFromUpTo10_ShouldBe23()
    {
        Assert.assertEquals(false, Challenge5_IsPrime.isPrime(0));
        Assert.assertEquals(false, Challenge5_IsPrime.isPrime(1));
        Assert.assertEquals(true, Challenge5_IsPrime.isPrime(2));
        Assert.assertEquals(true, Challenge5_IsPrime.isPrime(3));
        Assert.assertEquals(false, Challenge5_IsPrime.isPrime(4));
        Assert.assertEquals(true, Challenge5_IsPrime.isPrime(5));
        Assert.assertEquals(false, Challenge5_IsPrime.isPrime(6));
        Assert.assertEquals(true, Challenge5_IsPrime.isPrime(7));
        Assert.assertEquals(true, Challenge5_IsPrime.isPrime(13));
        Assert.assertEquals(false, Challenge5_IsPrime.isPrime(15));
        Assert.assertEquals(false, Challenge5_IsPrime.isPrime(36));
        Assert.assertEquals(true, Challenge5_IsPrime.isPrime(37));
    }

}
