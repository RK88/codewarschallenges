package junior.tests;

import junior.Challenge15_FoldArray;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class Test_Challenge15 {

    @Test
    public void basicTests() {
        int[] input = new int[]{1, 2, 3, 4, 5};
        int[] expected = new int[]{6, 6, 3};
        assertEquals(Arrays.toString(expected), Arrays.toString(Challenge15_FoldArray.foldArray(input, 1)));

        System.out.println(Arrays.toString(input));


        expected = new int[]{9, 6};
        assertEquals(Arrays.toString(expected), Arrays.toString(Challenge15_FoldArray.foldArray(input, 2)));

        System.out.println(Arrays.toString(input));



        expected = new int[]{15};
       assertEquals(Arrays.toString(expected), Arrays.toString(Challenge15_FoldArray.foldArray(input, 3)));

        input = new int[]{-9, 9, -8, 8, 66, 23};
        expected = new int[]{14, 75, 0};
        assertEquals(Arrays.toString(expected), Arrays.toString(Challenge15_FoldArray.foldArray(input, 1)));

    }
}