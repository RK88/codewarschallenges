package junior.tests;

import junior.Challenge17_ArraySortOdd;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class Test_Challenge17 {

    @Test
    public void exampleTest1() {
        assertArrayEquals(new int[]{ 1, 3, 2, 8, 5, 4 }, Challenge17_ArraySortOdd.sortArray(new int[]{ 5, 3, 2, 8, 1, 4 }));
    }

    @Test
    public void exampleTest2() {
        assertArrayEquals(new int[]{ 1, 3, 5, 8, 0 }, Challenge17_ArraySortOdd.sortArray(new int[]{ 5, 3, 1, 8, 0 }));
    }

    @Test
    public void exampleTest3() {
        assertArrayEquals(new int[]{}, Challenge17_ArraySortOdd.sortArray(new int[]{}));
    }
}
