package junior.tests;

import junior.Challenge7_FacebookLikes;
import org.junit.Assert;
import org.junit.Test;

public class Test_Challenge7 {
        @Test
        public void staticTests() {
            Assert.assertEquals("no one likes this", Challenge7_FacebookLikes.whoLikesIt());
            Assert.assertEquals("Peter likes this", Challenge7_FacebookLikes.whoLikesIt("Peter"));
            Assert.assertEquals("Jacob and Alex like this", Challenge7_FacebookLikes.whoLikesIt("Jacob", "Alex"));
            Assert.assertEquals("Max, John and Mark like this", Challenge7_FacebookLikes.whoLikesIt("Max", "John", "Mark"));
            Assert.assertEquals("Alex, Jacob and 2 others like this", Challenge7_FacebookLikes.whoLikesIt("Alex", "Jacob", "Mark", "Max"));
        }
}