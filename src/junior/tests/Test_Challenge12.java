package junior.tests;

import junior.Challenge12_ArrayUniqueNumber;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Test_Challenge12 {

    private double precision = 0.0000000000001;

    @Test
    public void sampleTestCases() {
        assertEquals(1.0, Challenge12_ArrayUniqueNumber.findUniq(new double[]{0, 1, 0}), precision);
    }

    @Test
    public void sampleTestCases2() {
        assertEquals(2.0, Challenge12_ArrayUniqueNumber.findUniq(new double[]{3,3, 3, 2}), precision);
    }

    @Test
    public void sampleTestCases3() {
        assertEquals(2.0, Challenge12_ArrayUniqueNumber.findUniq(new double[]{1, 1, 1, 2, 1, 1}), precision);
    }


}
