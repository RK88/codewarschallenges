package junior.tests;

import junior.Challenge9_CinemaTicketChange;
import org.junit.Assert;
import org.junit.Test;

public class Test_Challenge9 {


    @Test
    public void test1() {
        Assert.assertEquals("YES", Challenge9_CinemaTicketChange.Tickets(new int[] {25, 25, 50}));
    }
    @Test
    public void test2() {
        Assert.assertEquals("NO", Challenge9_CinemaTicketChange.Tickets(new int[] {25, 100}));
    }


}
