

package junior.tests;

import junior.Challenge16_DigitalRoot;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Test_Challenge16 {


    @Test
    public void Tests() {
        assertEquals( "Nope!" , 7, Challenge16_DigitalRoot.digital_root(16));
        assertEquals( "Nope!" , 6, Challenge16_DigitalRoot.digital_root(456));
    }

}
