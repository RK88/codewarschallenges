package junior.tests;

import junior.Challenge11_TripsDubs;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Test_Challenge11 {

    @Test
    public void test1(){
        assertEquals(1, Challenge11_TripsDubs.TripleDouble(451999277L, 41177722899L));
    }

    @Test
    public void test2(){
        assertEquals(0,  Challenge11_TripsDubs.TripleDouble(1222345L, 12345L));
    }

    @Test
    public void test3(){
        assertEquals(0, Challenge11_TripsDubs.TripleDouble(12345L, 12345L));
    }

    @Test
    public void test4() {
        assertEquals(1, Challenge11_TripsDubs.TripleDouble(666789L, 12345667L));
    }

    @Test
    public void test5() {
        assertEquals(0, Challenge11_TripsDubs.TripleDouble(451999277L, 411777228L));
    }

    @Test
    public void test6() {
        assertEquals(0, Challenge11_TripsDubs.TripleDouble(1112L, 122L));
    }

}
