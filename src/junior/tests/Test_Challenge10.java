package junior.tests;

import junior.Challenge10_CamelCaseBreak;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Test_Challenge10 {



    @Test
    public void tests() {
        assertEquals( "Incorrect", "camel Casing", Challenge10_CamelCaseBreak.camelCase("camelCasing"));
        assertEquals( "Incorrect", "camel Casing Test", Challenge10_CamelCaseBreak.camelCase("camelCasingTest"));
        assertEquals( "Incorrect", "camelcasingtest", Challenge10_CamelCaseBreak.camelCase("camelcasingtest"));
    }

}
