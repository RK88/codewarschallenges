package junior.tests;

import junior.Challenge1;
import org.junit.Assert;
import org.junit.Test;

public class Test_Challenge1 {

    @Test
    public void Challenge1_SumOfDividablesFromUpTo10_ShouldBe23()
    {
        int input = 10;
        int expected = 23;
        Assert.assertEquals(expected, Challenge1.solution(input));
    }
}
