package junior.tests;

import junior.Challenge14_ArrayLengthSort;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Test_Challenge14 {

    @Test
    public void BasicTests() {
        assertEquals(3, Challenge14_ArrayLengthSort.getLengthOfMissingArray(new Object[][] { new Object[] { 1, 2 }, new Object[] { 4, 5, 1, 1 }, new Object[] { 1 }, new Object[] { 5, 6, 7, 8, 9 }} ));
    }

    public void BasicTests2() {
        assertEquals(2, Challenge14_ArrayLengthSort.getLengthOfMissingArray(new Object[][] { new Object[] { 5, 2, 9 }, new Object[] { 4, 5, 1, 1 }, new Object[] { 1 }, new Object[] { 5, 6, 7, 8, 9 }} ));
    }
    public void BasicTests3() {
        assertEquals(2, Challenge14_ArrayLengthSort.getLengthOfMissingArray(new Object[][] { new Object[] { null }, new Object[] { null, null, null } } ));
    }
    public void BasicTests4() {
        assertEquals(5, Challenge14_ArrayLengthSort.getLengthOfMissingArray(new Object[][] { new Object[] { 'a', 'a', 'a' }, new Object[] { 'a', 'a' }, new Object[] { 'a', 'a', 'a', 'a' }, new Object[] { 'a' }, new Object[] { 'a', 'a', 'a', 'a', 'a', 'a' }} ));
    }
    public void BasicTests5() {
        assertEquals(0, Challenge14_ArrayLengthSort.getLengthOfMissingArray(new Object[][] { }));
    }
}
