package junior.tests;

import junior.Challenge8_StringSplitPairs;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class Test_Challenge8 {

    @Test
    public void testEvenString() {
        String s = "abcdef";
        String s1 = "HelloWorld";
        Assert.assertEquals("Should handle even string","[ab, cd, ef]", Arrays.toString(Challenge8_StringSplitPairs.solution(s)));
        Assert.assertEquals("Should handle even string","[He, ll, oW, or, ld]", Arrays.toString(Challenge8_StringSplitPairs.solution(s1)));
    }


}
