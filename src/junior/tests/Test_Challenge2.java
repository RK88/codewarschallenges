package junior.tests;

import junior.Challenge2_PrinterErrors;
import org.junit.Assert;
import org.junit.Test;

public class Test_Challenge2 {

    @Test
    public void Challenge1_SumOfDividablesFromUpTo10_ShouldBe23()
    {
        String input = "xxxaaaabbcczz";
        String expected = "5/13";
        Assert.assertEquals(expected, Challenge2_PrinterErrors.printerError(input));
    }
}
