

package junior.tests;

import junior.Challenge18_TimeFormatter;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Test_Challenge18 {

    @Test
    public void exampleTests() {
        assertEquals("1 second", Challenge18_TimeFormatter.formatDuration(1));
        assertEquals("1 minute and 2 seconds", Challenge18_TimeFormatter.formatDuration(62));
        assertEquals("2 minutes", Challenge18_TimeFormatter.formatDuration(120));
        assertEquals("1 hour", Challenge18_TimeFormatter.formatDuration(3600));
        assertEquals("1 hour, 1 minute and 2 seconds", Challenge18_TimeFormatter.formatDuration(3662));
    }


}
