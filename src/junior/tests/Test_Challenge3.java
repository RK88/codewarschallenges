package junior.tests;

import junior.Challenge3_CamelPascal;
import org.junit.Assert;
import org.junit.Test;

public class Test_Challenge3 {

    @Test
    public void Challenge1_SumOfDividablesFromUpTo10_ShouldBe23()
    {
        Assert.assertEquals("theStealthWarrior", Challenge3_CamelPascal.toCamelCase("the-stealth-warrior"));
        Assert.assertEquals("TheStealthWarrior", Challenge3_CamelPascal.toCamelCase("The-stealth-warrior"));
        Assert.assertEquals("theStealthWarrior", Challenge3_CamelPascal.toCamelCase("the_Stealth_Warrior"));
        Assert.assertEquals("", Challenge3_CamelPascal.toCamelCase(""));
    }
}
