package junior.tests;

import junior.Challenge4_Braces;
import org.junit.Assert;
import org.junit.Test;

public class Test_Challenge4 {

    @Test
    public void Challenge1_SumOfDividablesFromUpTo10_ShouldBe23()
    {


        Assert.assertEquals(true, Challenge4_Braces.isValid("(){}[]"));
        Assert.assertEquals(true, Challenge4_Braces.isValid("([{}])"));
        Assert.assertEquals(false, Challenge4_Braces.isValid("(}"));
        Assert.assertEquals(false, Challenge4_Braces.isValid("[(])"));
        Assert.assertEquals(false, Challenge4_Braces.isValid("[({})](]"));
        Assert.assertEquals(false, Challenge4_Braces.isValid("[]["));
        Assert.assertEquals(true, Challenge4_Braces.isValid("[{{}}]"));

        Assert.assertEquals(true, Challenge4_Braces.isValid("[[[[]]]]"));
        Assert.assertEquals(true, Challenge4_Braces.isValid("[{({})}]"));
        Assert.assertEquals(true, Challenge4_Braces.isValid("[{[{(([]))}]}]"));
        Assert.assertEquals(true, Challenge4_Braces.isValid("[]"));

        Assert.assertEquals(false, Challenge4_Braces.isValid("["));
        Assert.assertEquals(false, Challenge4_Braces.isValid("[{[{([]))}]}]"));
        Assert.assertEquals(false, Challenge4_Braces.isValid(""));

        Assert.assertEquals(true, Challenge4_Braces.isValid("(({{[[]]}}))"));
        Assert.assertEquals(true, Challenge4_Braces.isValid("({})({})"));  //<------ here

    }
}
