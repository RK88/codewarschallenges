//Complete the solution so that it splits the string into pairs of two characters.
// If the string contains an odd number of characters then it should replace the missing second character of the final pair with an underscore ('_').
//
//        Examples:
//
//        StringSplit.solution("abc") // should return {"ab", "c_"}
//        StringSplit.solution("abcdef") // should return {"ab", "cd", "ef"}


package junior;

import java.util.ArrayList;
import java.util.List;

public class Challenge8_StringSplitPairs {

    public static void main(String[] args) {

        String[] lord = solution("abcdef");

        for (String s : lord) {
            System.out.println(s);
        }

    }

    public static String[] solution(String s) {
        int lenght = s.length();
        List<String> sb = new ArrayList<>();

        if (lenght % 2 == 0) {
            for (int i = 0; i < (lenght / 2); i++) {
                sb.add(s.substring(i * 2, (i * 2) + 2));
            }
        } else {
            s = s.concat("_");
            for (int i = 0; i < ((lenght + 1) / 2); i++) {
                sb.add(s.substring(i * 2, (i * 2) + 2));
            }
        }
        return sb.toArray(new String[0]);
    }
}
