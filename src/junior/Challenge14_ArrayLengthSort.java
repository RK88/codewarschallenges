//You get an array of arrays.
//        If you sort the arrays by their length, you will see, that their length-values are consecutive.
//        But one array is missing!
//
//
//        You have to write a method, that return the length of the missing array.
//
//        Example:
//        [[1, 2], [4, 5, 1, 1], [1], [5, 6, 7, 8, 9]] --> 3
//
//        If the array of arrays is null/nil or empty, the method should return 0.
//
//        When an array in the array is null or empty, the method should return 0 too!
//        There will always be a missing element and its length will be always between the given arrays.
//
//        Have fun coding it and please don't forget to vote and rank this kata! :-)
//
//        I have created other katas. Have a look if you like coding and challenges.


package junior;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Challenge14_ArrayLengthSort {

    public static void main(String[] args) {
        int l = getLengthOfMissingArray(new Object[][]{new Object[]{1, 2}, new Object[]{4, 5, 1, 1}, new Object[]{1}, new Object[]{5, 6, 7, 8, 9}});
        System.out.println("===");
        System.out.println(l);
    }

    public static int getLengthOfMissingArray(Object[][] arrayOfArrays) {

        int length = Array.getLength(arrayOfArrays);

        int[] lArray = new int[length];

        for (int i = 0; i < length ; i++) {
            lArray[i] = Array.getLength(arrayOfArrays[i]);
        }

        Arrays.sort(lArray);

        for (int i = 0; i < lArray.length-2; i++) {
            int currValue = lArray[i];
            int nextValue = lArray[i+1];

            if (nextValue - currValue >1)
            {
                return currValue+1;
            }
        }

        return lArray[lArray.length-1];
    }
}
