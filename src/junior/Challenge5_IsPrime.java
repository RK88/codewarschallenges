//Define a function that takes an integer argument and returns logical value true or false depending on if the integer is a prime.
//
//        Per Wikipedia, a prime number (or a prime) is a natural number greater than 1 that has no positive divisors
//        other than 1 and itself.
//
//        Example
//        is_prime(1)  /* false */
//        is_prime(2)  /* true  */
//        is_prime(-1) /* false */


package junior;

public class Challenge5_IsPrime {


    public static void main (String[] args)
    {
        for (int i = 0; i <40; i ++)
        {
            System.out.println((isPrime(i)?i+" is prime number": i+ " is not prime number"));
        }
    }


    public static boolean isPrime(int num) {
        if (num <=1) return false;
        int squaredNumber = (int) Math.floor(Math.sqrt(num));


        for (int i = 2; i <(squaredNumber+1); i++)
        {
            if (num%i == 0) return false;
        }

        return true;
    }
}
