//You probably know the "like" system from Facebook and other pages. People can "like" blog posts, pictures or other items.
// We want to create the text that should be displayed next to such an item.
//
//        Implement a function likes :: [String] -> String, which must take in input array, containing the names of people who like an item.
//        It must return the display text as shown in the examples:
//
//        likes {} // must be "no one likes this"
//        likes {"Peter"} // must be "Peter likes this"
//        likes {"Jacob", "Alex"} // must be "Jacob and Alex like this"
//        likes {"Max", "John", "Mark"} // must be "Max, John and Mark like this"
//        likes {"Alex", "Jacob", "Mark", "Max"} // must be "Alex, Jacob and 2 others like this"



package junior;

public class Challenge7_FacebookLikes {

    private final static String likeThis = " like this";
    private final static String likesThis = " likes this";
    private final static String nobodyLikes = "no one likes this";

    public static void main (String[] args)
    {
        System.out.println(whoLikesIt("Jacob", "Alex"));
    }


    public static String whoLikesIt(String... names) {
        return (names.length==0 ? nobodyLikes : processLikes(names));
    }

    private static String processLikes(String[] names) {
        int lenght = names.length;

        switch (lenght)
        {
            case 1:
                return names[0] + " likes this";
            case 2:
                return names[0] + " and " + names[1] + likeThis;
            case 3:
                return names[0] + ", " + names[1] + " and " + names[2] + likeThis;
            default: //more than 3
                StringBuilder buildAffixString = new StringBuilder();
                buildAffixString.append(names[0] + ", ");
                buildAffixString.append(names[1]);
                buildAffixString.append(" and " + (lenght-2) + " others like this");
                return buildAffixString.toString();

        }
    }
}
