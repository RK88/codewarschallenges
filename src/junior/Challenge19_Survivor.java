//In this kata you have to correctly return who is the "survivor", ie: the last element of a Josephus permutation.
//
//        Basically you have to assume that n people are put into a circle and that they are eliminated in steps of k elements, like this:
//
//        josephus_survivor(7,3) => means 7 people in a circle;
//        one every 3 is eliminated until one remains
//        [1,2,3,4,5,6,7] - initial sequence
//        [1,2,4,5,6,7] => 3 is counted out
//        [1,2,4,5,7] => 6 is counted out
//        [1,4,5,7] => 2 is counted out
//        [1,4,5] => 7 is counted out
//        [1,4] => 5 is counted out
//        [4] => 1 counted out, 4 is the last element - the survivor!
//


//=========================================================================

//simple solution was :
//
//        if(n == 1)
//        return 1;
//
//        return (josephusSurvivor(n - 1, k) + k - 1) % n + 1;
//



package junior;

import java.util.Arrays;

public class Challenge19_Survivor {


    public static void main(String[] args) {
        int[] array = fillArray(10);
        josephusSurvivor(3, 7);
    }


    public static int josephusSurvivor(final int n, final int k) {
        int[] survivorsArr = fillArray(n);
        return cull(survivorsArr, k)[0];
    }

    public static int[] cull(int[] survivorsArr, int k) {

        int counter = k-1 ;

        if (k == 1) {
            return new int[]{survivorsArr.length};
        }

        while (survivorsArr.length != 1) {

            while (survivorsArr.length -1 < counter) {
                counter = counter - survivorsArr.length;
            }

            for (int i = 0; i < survivorsArr.length; i++) {
                if (counter == i) {
                    survivorsArr[i] = 0;
                    counter += k;
                }
                System.out.println(Arrays.toString(survivorsArr));
                System.out.println("counter=" + counter);
            }

            if (survivorsArr.length - 1 < counter) {
                counter = counter - survivorsArr.length;
            }

            survivorsArr = Arrays.stream(survivorsArr).filter(x -> x != 0).toArray();
        }


        int[] result = Arrays.stream(survivorsArr).filter(x -> x != 0).toArray();
        System.out.println("result = " + Arrays.toString(result));
        return result;
    }


    private static int[] fillArray(int n) {
        int[] result = new int[n];
        for (int i = 1; i != n + 1; i++) {
            result[i - 1] = i;
        }
        return result;
    }
}
