//Move the first letter of each word to the end of it, then add "ay" to the end of the word. Leave punctuation marks untouched.
//
//        Examples
//        pigIt('Pig latin is cool'); // igPay atinlay siay oolcay
//        pigIt('Hello world !');     // elloHay orldway !


package junior;

public class Challenge13_StringPartMoveAy {

    public static void main(String[] args) {
        String pig = "Pig aaaa";
        System.out.println(pigIt(pig));
    }


    public static String pigIt(String str) {

        String[] strArr = str.split(" ");

        StringBuilder sb = new StringBuilder();

        for (String s : strArr)
        {
            sb.append(pig(s));
            sb.append(" ");
        }
        return sb.toString().trim();
    }


    public static String pig(String str)
    {
        int length = str.length();
        String affix = str.substring(0, 1).concat("ay");
        String result = str.substring(1, length) + affix;
        return result;
    }
}
