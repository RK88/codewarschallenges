
//In a factory a printer prints labels for boxes. For one kind of boxes the printer has to use colors which, for the sake of simplicity, are named with letters from a to m.
//        The colors used by the printer are recorded in a control string. For example a "good" control string would be aaabbbbhaijjjm meaning that the printer used three times color a, four times color b, one time color h then one time color a...
//        Sometimes there are problems: lack of colors, technical malfunction and a "bad" control string is produced e.g. aaaxbbbbyyhwawiwjjjwwm with letters not from a to m.
//        You have to write a function printer_error which given a string will output the error rate of the printer as a string representing a rational whose numerator is the number of errors and the denominator the length of the control string. Don't reduce this fraction to a simpler expression
//        The string has a length greater or equal to one and contains only letters from ato z.
//        #Examples:
//
//        s="aaabbbbhaijjjm"
//        error_printer(s) => "0/14"
//
//        s="aaaxbbbbyyhwawiwjjjwwm"
//        error_printer(s) => "8/22"

package junior;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Challenge2_PrinterErrors {

    public static void main(String[] args) {
        System.out.println(printerError("ax"));
    }


    public static String printerError(String s) {
        //sorting characters first
        String newString = sortChars(s);

        int length = s.length();
        int firstIllegalPos = findFirstIllegal(newString);
        int numberOfErrors = 0;
        if (firstIllegalPos != -1) {
            numberOfErrors = length - firstIllegalPos;
        }

        return numberOfErrors + "/" + length;
    }

    //returns index of first letter above m
    public static int findFirstIllegal(String str) {
        for (int i = 0; i < str.length(); i++) {
            char currentChar = str.charAt(i);
            if (currentChar >= 110) {
                return i;
            }
        }
        return -1;
    }

    public static String sortChars(String str)
    {
        String result =
                Stream.of(str.split(""))
                        .sorted()
                        .collect(Collectors.joining());
        return result;
    }

}
