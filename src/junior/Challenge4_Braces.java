//Write a function that takes a string of braces, and determines if the order of the braces is valid.
//It should return true if the string is valid, and false if it's invalid.
//
//This Kata is similar to the Valid Parentheses Kata, but introduces new characters: brackets [], and curly braces {}.
//Thanks to @arnedag for the idea!
//
//All input strings will be nonempty, and will only consist of parentheses, brackets and curly braces: ()[]{}.
//
//What is considered Valid?
//A string of braces is considered valid if all braces are matched with the correct brace.
//
//        Examples
//        "(){}[]"   =>  True
//        "([{}])"   =>  True
//        "(}"       =>  False
//        "[(])"     =>  False
//        "[({})](]" =>  False


package junior;

import java.util.ArrayList;
import java.util.List;

public class Challenge4_Braces {

    public static void main(String[] str) {
        System.out.println(isValid("({})({})"));
    }

    public static boolean isValid(String braces) {


        if (braces.length() == 0) return false;

        boolean firstCheck = false;
        boolean secondCheck = false;

        char[] ch = braces.toCharArray();

        //check if we have same amount of openings and closings
        firstCheck = doAllHaveClosings(ch, 0, ch.length);
        secondCheck = iterateString(braces);


        if (!firstCheck || !secondCheck) return false;
        return true;
    }



    private static boolean iterateString(String st)
    {
        int start = 0;
        int end = st.length()-1;
        while (end>start)
        {
            int lastFoundClosing = end;
            int foundClosingPlace = hasClosingInRange(st.toCharArray(), start, end);

            if (foundClosingPlace == -1) return false;

            start++;
            end = foundClosingPlace;

            if (foundClosingPlace == lastFoundClosing) end --;

        }
        return true;
    }


    //searches for closing symbol from end, and returns position once found. Returns -1 if some shit happens
    private static int hasClosingInRange(char[] chArr, int start, int end) {

        if (start >= end || start < 0 || end > chArr.length) return -1;

        char currentSymbol = chArr[start];
        char expectedEndingSymbol = '0';

        switch (currentSymbol) {
            case '(':
                expectedEndingSymbol = ')';
                break;
            case '{':
                expectedEndingSymbol = '}';
                break;
            case '[':
                expectedEndingSymbol = ']';
                break;
            default:
                return -1;
        }


        //backwards loop looking for closing symbol
        for (int i = end; i > start; i--) {
            if (chArr[i] == expectedEndingSymbol) return i;
        }
        return -1;
    }


    private static boolean doAllHaveClosings(char[] chArr, int start, int end) {
        int[] expectedClosings = {0, 0, 0}; // ( { [

        for (int i = start; i < end; i++) {
            char currChar = chArr[i];
            switch (currChar) {
                case '(':
                    expectedClosings[0]++;
                    break;
                case '{':
                    expectedClosings[1]++;
                    break;
                case '[':
                    expectedClosings[2]++;
                    break;
                case ')':
                    expectedClosings[0]--;
                    //else return false;
                    break;
                case '}':
                     expectedClosings[1]--;
                   // else return false;
                    break;
                case ']':
                     expectedClosings[2]--;
                   // else return false;
                    break;
            }
        }
        return ((expectedClosings[0] == 0 && expectedClosings[1] == 0 && expectedClosings[2] == 0) ? true : false);
    }
}
