//Complete the method/function so that it converts dash/underscore delimited words into camel casing.
//The first word within the output should be capitalized only if the original word was capitalized (known as Upper Camel Case, also often referred to as Pascal case).
//
//        Examples
//        toCamelCase("the-stealth-warrior"); // returns "theStealthWarrior"
//
//        toCamelCase("The_Stealth_Warrior"); // returns "TheStealthWarrior"


package junior;

public class Challenge3_CamelPascal {

    public static void main(String[] args) {
        System.out.println(toCamelCase("the-stealth-warrior"));
    }

    public static String toCamelCase(String s) {
        if (s.length() ==0) return "";
        String[] words = s.split("[^a-zA-Z0-9]");
        if (words.length == 0) return "";

        String result = "";

        int start = (Character.isUpperCase(words[0].charAt(0)) ? 0 : 1);
        for (int i = start; i < words.length; i++) {
            char[] chArr = words[i].toCharArray();
            chArr[0] = Character.toUpperCase(words[i].charAt(0));
            words[i] = String.valueOf(chArr);
        }
        result = String.join("",words);
        return result;

    }
}
