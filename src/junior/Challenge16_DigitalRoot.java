//
//In this kata, you must create a digital root function.
//
//        A digital root is the recursive sum of all the digits in a number. Given n, take the sum of the digits of n.
//        If that value has more than one digit, continue reducing in this way until a single-digit number is produced.
//        This is only applicable to the natural numbers.
//
//        Here's how it works:

//
//digital_root(16)
//        => 1 + 6
//        => 7
//
//        digital_root(942)
//        => 9 + 4 + 2
//        => 15 ...
//        => 1 + 5
//        => 6


package junior;

public class Challenge16_DigitalRoot {

    public static void main (String[] str)
    {
        System.out.println(digital_root(942));
      //  System.out.println((5/10)%10);
    }

    public static int digital_root(int n) {

        while (((n/10)%10)>0)
        {
            n = sumOfNumbers(n);
        }

      return n;
    }

    public static int sumOfNumbers(int n)
    {
        int currentSum = 0;

        while(n/10 > 0)
        {
            currentSum+= n%10;
            n = n / 10;
        }
        currentSum+=n%10;

        return currentSum;
    }
}
