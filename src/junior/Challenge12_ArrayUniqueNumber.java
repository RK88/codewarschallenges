//
//There is an array with some numbers. All numbers are equal except for one. Try to find it!
//
//        Kata.findUniq(new double[]{ 1, 1, 1, 2, 1, 1 }); // => 2
//        Kata.findUniq(new double[]{ 0, 0, 0.55, 0, 0 }); // => 0.55
//        It’s guaranteed that array contains at least 3 numbers.
//
//        The tests contain some very huge arrays, so think about performance.

package junior;

import java.util.Arrays;

public class Challenge12_ArrayUniqueNumber {

    public static void main(String[] args) {

    }

    public static double findUniq(double arr[]) {

        double unique = 0;
        Arrays.sort(arr);
        if (arr[0]!=arr[1] && arr[0]!=arr[2])
        {
            unique = arr[0];
        }
        else
        {
            if (arr[arr.length-1] != arr[arr.length-2] && arr[arr.length-1] != arr[arr.length-3])
            {
                unique = arr[arr.length-1];
            }
        }

        return unique;
    }
}
