//You have an array of numbers.
//        Your task is to sort ascending odd numbers but even numbers must be on their places.
//
//        Zero isn't an odd number and you don't need to move it. If you have an empty array, you need to return it.
//
//        Example
//
//        sortArray([5, 3, 2, 8, 1, 4]) == [1, 3, 2, 8, 5, 4]


package junior;

import java.util.Arrays;

public class Challenge17_ArraySortOdd {


    public static void main(String[] args) {
        int[] arr = new int[]{5, 3, 2, 8, 1, 4};
        sortArray(arr);
        System.out.println(Arrays.toString(arr));
    }


    public static int[] sortArray(int[] array) {

        int[] oddsOnly = getOdds(array);
        Arrays.sort(oddsOnly);

        int currentOdd = 0;
        for (int i = 0; i < array.length; i++)
        {
            if (isOdd(array[i]))
            {
                array[i] = oddsOnly[currentOdd];
                currentOdd ++;
            }
        }

        return array;
    }

    public static int[] getOdds(int[] inputArr) {
        return Arrays.stream(inputArr).filter(x -> x % 2 != 0).toArray();
    }


    public static boolean isOdd(int number) {
        return (number % 2 != 0);
    }

}
