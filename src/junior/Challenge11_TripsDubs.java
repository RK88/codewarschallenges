
/*
        which takes numbers num1 and num2 and returns 1 if there is a straight triple of a number at any place in num1 and also a straight double of the same number in num2.

        If this isn't the case, return 0

        Examples
        TripleDouble(451999277, 41177722899) == 1 // num1 has straight triple 999s and
        // num2 has straight double 99s

        TripleDouble(1222345, 12345) == 0 // num1 has straight triple 2s but num2 has only a single 2

        TripleDouble(12345, 12345) == 0

        TripleDouble(666789, 12345667) == 1
*/


package junior;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Challenge11_TripsDubs {

    public static void main(String[] args) {
        System.out.println(TripleDouble(12345L, 12345L));
    }

    public static int TripleDouble(long num1, long num2) {
        boolean firstMatch = false;
        boolean secondMatch = false;


        String numStr1 = String.valueOf(num1);
        Pattern pattern = Pattern.compile("[1]{3,}|[2]{3,}|[3]{3,}|[4]{3,}|[5]{3,}|[6]{3,}|[7]{3,}|[8]{3,}|[9]{3,}|[0]{3,}");
        Matcher matcher = pattern.matcher(numStr1);

        String lookingForNumber = "@";

        if (matcher.find()) {
            MatchResult mr = matcher.toMatchResult();
            System.out.println(mr.group(0));
            lookingForNumber = mr.group(0).substring(0,1);
            firstMatch = true;
        }

        String numStr2 = String.valueOf(num2);

        String newPatternString = "";
        newPatternString = newPatternString.concat("[").concat(lookingForNumber).concat("]").concat("{2,}");

        Pattern pattern2 = Pattern.compile(newPatternString);
        Matcher matcher2 = pattern2.matcher(numStr2);

        if (matcher2.find()) {
            secondMatch = true;
        }
        return (firstMatch && secondMatch ? 1 : 0);

    }


}
