//Fold 1-times:
//        [1,2,3,4,5] -> [6,6,3]
//
//        A little visualization (NOT for the algorithm but for the idea of folding):
//
//        Step 1         Step 2        Step 3       Step 4       Step5
//        5/           5|         5\
//        4/            4|          4\
//        1 2 3 4 5      1 2 3/         1 2 3|       1 2 3\       6 6 3
//        ----*----      ----*          ----*        ----*        ----*
//
//
//        Fold 2-times:
//        [1,2,3,4,5] -> [9,6]
//        As you see, if the count of numbers is odd, the middle number will stay. Otherwise the fold-point is between the middle-numbers, so all numbers would be added in a way.
//
//        The array will always contain numbers and will never be null. The parameter runs will always be a positive integer greater than 0 and says how many runs of folding your method has to do.
//
//        If an array with one element is folded, it stays as the same array.
//
//        The input array should not be modified!


package junior;

import java.util.Arrays;

public class Challenge15_FoldArray {

    public static void main(String[] args) {

        int[] input = {1, 2, 3, 4, 5};

        int[] result = foldArray(Arrays.copyOfRange(input,0,input.length), 2); //9, 6
        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i]);
        }
    }

    public static int[] foldArray(int[] array, int runs) {

        int mid = array.length / 2;
        int[] result = new int[]{};
        int[] curArray = array;

        for (int j = 0; j < runs; j++) {
            for (int i = 0; i < mid; i++) {
                curArray[i] += curArray[curArray.length - 1 - i];
            }
            result = (curArray.length % 2 == 0 ? Arrays.copyOfRange(curArray, 0, mid) : Arrays.copyOfRange(curArray, 0, mid + 1));
            curArray = result;
            mid = curArray.length / 2;
        }
        return result;
    }
}