//Complete the solution so that the function will break up camel casing, using a space between words.
//
//        Example
//        solution("camelCasing")  ==  "camel Casing"


//=== best solution was ===

//import java.util.Arrays;
//
//class Solution {
//    public static String camelCase(String input) {
//        return input.replaceAll("([A-Z])", " $1");
//    }
//}

package junior;

public class Challenge10_CamelCaseBreak {

    public static void main(String[] args) {
        System.out.println(camelCase("camelCasingTest"));
    }


    public static String camelCase(String input) {

        // String[] result = input.split("(?<=[A-Z])");
        String[] result = input.split("(?=[A-Z])");
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < result.length; i ++) {
            sb.append(result[i]);
            sb.append(" ");
        }
        return sb.toString().trim();
    }
}
