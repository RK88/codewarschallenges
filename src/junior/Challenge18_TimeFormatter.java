//Your task in order to complete this Kata is to write a function which formats a duration, given as a number of seconds, in a human-friendly way.
//
//        The function must accept a non-negative integer. If it is zero, it just returns "now".
//        Otherwise, the duration is expressed as a combination of years, days, hours, minutes and seconds.
//
//        It is much easier to understand with an example:
//
//        TimeFormatter.formatDuration(62)   //returns "1 minute and 2 seconds"
//        TimeFormatter.formatDuration(3662) //returns "1 hour, 1 minute and 2 seconds"


package junior;

public class Challenge18_TimeFormatter {

    public static void main(String[] args) {
        String years = formatDuration(132030240);
        System.out.println(years);
    }


    public static String formatDuration(int seconds) {

        if (seconds==0) return "now";

        int yearseconds = ((3600 * 24 * 365) + (24 * 3600));
        int dayseconds = (24 * 3600);
        int hourseconds = 60 * 60;
        int minuteseconds = 60;

        int years = (int) seconds / yearseconds;
        seconds -= years * yearseconds;

        int days = (int) seconds / dayseconds;
        seconds -= dayseconds * days;

        int hours = (int) seconds / hourseconds;
        seconds -= hourseconds * hours;

        int minutes = (int) seconds / minuteseconds;
        seconds -= minuteseconds * minutes;


        StringBuilder sb = new StringBuilder();

        if (years > 0) {
            sb.append(years == 1 ? String.valueOf(years + " year") : String.valueOf(years + " years"));
            sb.append(", ");
        }

        if (days > 0) {
            sb.append(days == 1 ? String.valueOf(days + " day") : String.valueOf(days + " days"));
            sb.append(", ");
        }

        if (hours > 0) {
            sb.append(hours == 1 ? String.valueOf(hours + " hour") : String.valueOf(hours + " hours"));
            sb.append(", ");
        }

        if (minutes > 0) {
            sb.append(minutes == 1 ? String.valueOf(minutes + " minute") : String.valueOf(minutes + " minutes"));
            sb.append(", ");
        }

        if (seconds > 0) {
            sb.append(seconds == 1 ? String.valueOf(seconds + " second") : String.valueOf(seconds + " seconds"));
            sb.append(", ");
        }

        String result = sb.toString();

        result.trim();

        if (result.substring(result.length() - 2, result.length() - 1).equals(",")) {
            result = result.substring(0, result.length() - 2);
        }

        long count = result.chars().filter(ch -> ch == ',').count();

        switch ((int)count)
        {
            case 0:
                break;
            case 1:
                result = result.replace(", "," and ");
                break;
            default:
                int lastComaIndex = result.lastIndexOf(", ");
                String latestPart = result.substring(lastComaIndex, result.length()).replace(", ", " and ");
                result = result.substring(0,lastComaIndex).concat(latestPart);
        }

         return result;
    }
}
